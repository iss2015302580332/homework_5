import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WeiZehao on 16/12/17.
 * 本类是数据库管理，用于插入和查询数据库数据
 */
public class Database
{
    private static final String url = "jdbc:mysql://localhost:3306/videoData?" +
            "useUnicode=true&characterEncoding=utf-8&useSSL=false" +
            "&useServerPrepStmts=false&rewriteBatchedStatements=true";
    private Connection con;
    private ResultSet resultSet;
    private PreparedStatement preStmt;

    /**
     * connect to database
     */
    public Database()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url,"root","");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * close connection
     */
    public void close()
    {
        try
        {
            con.close();
            preStmt.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 插入数据
     * @param videos 包含视频信息的Video对象
     */
    public void insertData(ArrayList<Video> videos)
    {
        String sql = "INSERT INTO videoInfo(aid,tid,tname,author,coin,favorite) VALUES(?,?,?,?,?,?)";
        try
        {
            preStmt = con.prepareStatement(sql);
            con.setAutoCommit(false);
            for(int i = 0; i < videos.size(); i++)
            {
                preStmt.setInt(1,videos.get(i).aid);
                preStmt.setInt(2,videos.get(i).tid);
                preStmt.setString(3,videos.get(i).tname);
                preStmt.setString(4,videos.get(i).author);
                preStmt.setInt(5,videos.get(i).coin);
                preStmt.setInt(6,videos.get(i).favorite);
                preStmt.addBatch();
            }
            preStmt.executeBatch();
            con.commit();
            System.out.println("One finish");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 获取TOP3视频的相应数据
     * @param tid 视频所属类别
     * @return 储存视频信息对象数组
     */
    public Video[] getData(int tid)
    {
        String sql = String.format("SELECT * FROM videoInfo WHERE tid=%d ORDER BY favorite DESC LIMIT 3", tid);
        Video[] videos = new Video[3];
        try
        {
            preStmt = con.prepareStatement(sql);
            resultSet = preStmt.executeQuery();
            for(int i = 0; i < 3; i++)
            {
                resultSet.next();
                int aid = resultSet.getInt(1);
                String tname = resultSet.getString(3);
                String author = resultSet.getString(4);
                int coin = resultSet.getInt(5);
                int favorite = resultSet.getInt(6);
                videos[i] = new Video(aid,tid,tname,author,coin,favorite);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return videos;
    }
}

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by WeiZehao on 16/12/24.
 * 本类用于筛选出每一类的TOP3视频
 * 获取所有TOP3视频的URL
 * 下载所有的TOP3视频
 */
public class VideoGetter
{
    private static final String basicURL = "http://www.ibilibili.com/video/av";
    private Database database;

    public VideoGetter()
    {
        database = new Database();
    }

    /**
     * 批量下载视频
     */
    public void batchDownload(String path)
    {
        File file = new File(path);
        Video[] videos;
        try
        {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext())
            {
                int tid = scanner.nextInt();
                System.out.println(String.format("--------获取类别：%d--------", tid));
                // 从数据库中获取TOP3视频信息
                videos = database.getData(tid);
                for(Video v : videos)
                {
                    // 获得视频地址
                    String url = this.analyseHTML(this.getRequest(basicURL + v.aid + "/"));
                    System.out.println(url);
                    String info = String.format("aid:%d tid:%d tname:%s author:%s coin:%d favorite:%d",
                            v.aid, v.tid, v.tname, v.author, v.coin, v.favorite);
                    System.out.println("--------准备下载视频--------");
                    System.out.println(info);

                    // 尝试下载2次，第1次成功则break，否则尝试第二次
                    for(int i = 0; i < 2; i++)
                    {
                        if(this.downloadVideo(url, v.tid, v.aid))
                            break;
                        else
                            System.out.println("下载失败\n");
                    }
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 下载视频
     * @param _url 视频地址
     * @param tid  视频类别
     * @param aid  视频号
     * @return 是否下载成功
     */
    public boolean downloadVideo(String _url, int tid, int aid)
    {
        FileOutputStream fileout = null;
        try
        {
            // 构建连接，设置响应等待时间
            URL url = new URL(_url);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setReadTimeout(10000);

            if(con.getResponseCode()==HttpURLConnection.HTTP_OK)
            {
                System.out.println("开始下载");
                InputStream is = con.getInputStream();
                fileout = new FileOutputStream(new File(String.format("../download/tid%d_av%d.mp4", tid, aid)));

                // 写文件
                byte[] b = new byte[1024];
                int len = 0 ;
                while((len = is.read(b)) != -1)
                {
                    fileout.write(b, 0, len);
                }
                fileout.flush();
                System.out.println("下载完成\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        finally
        {
            if(fileout!=null)
            {
                try
                {
                    fileout.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 请求html文件，获取回应
     * @param url 网页地址
     * @return  储存在response中的html信息
     */
    private HttpResponse getRequest(String url)
    {
        HttpResponse response = null;
        try
        {
            HttpClient httpClient = HttpClients.createDefault();
            HttpGet request = new HttpGet(url);
            request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            request.addHeader("Accept-Encoding", "gzip, deflate, sdch");
            request.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6");
            request.addHeader("Cache-Control", "max-age=0");
            request.addHeader("Connection", "keep-alive");
            request.addHeader("Host", "www.ibilibili.com");
            request.addHeader("Upgrade-Insecure-Requests", "1");
            request.addHeader("Content-Type", "text/html; charset=UTF-8");
            request.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " +
                    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36");
            response = httpClient.execute(request);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * 提取视频下载地址
     * @param response html信息
     * @return 视频地址
     */
    private String analyseHTML(HttpResponse response)
    {
        if(response == null)
            System.out.println("Fail");

        String url = "";
        HttpEntity resEntity = response.getEntity();

        try
        {
            // 获取含视频地址的标签
            String content = EntityUtils.toString(resEntity, "utf-8");
            Pattern p = Pattern.compile("<a.*?>视频下载.*</a>");
            Matcher m = p.matcher(content);
            m.find();
            content = m.group(0);
            System.out.println(m.group(0));

            // 从标签中获取视频地址
            p = Pattern.compile("(?<=href =\").*(?=\" >)");
            m = p.matcher(content);
            m.find();
            url = m.group(0);
            System.out.println("已获得视频下载地址");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return url;
    }
}


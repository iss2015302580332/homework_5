import java.util.concurrent.CyclicBarrier;

/**
 * Created by WeiZehao on 16/12/31.
 * 本类用于获取TOP3视频并下载
 */
public class MainB
{
    public static void main(String[] args)
    {
        DownloadThread thread1 = new DownloadThread("doc/tid2.txt");
        DownloadThread thread2 = new DownloadThread("doc/tid3.txt");
        DownloadThread thread3 = new DownloadThread("doc/tid4.txt");
        DownloadThread thread4 = new DownloadThread("doc/tid5.txt");

        CyclicBarrier barrier = new CyclicBarrier(5);

        try
        {
            thread1.start();
            thread2.start();
            thread3.start();
            thread4.start();

            barrier.await();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("All threads accomplish");
    }
}

class DownloadThread extends Thread
{
    private String path;

    public DownloadThread(String _path)
    {
        path = _path;
    }

    @Override
    public void run()
    {
        VideoGetter videoGetter = new VideoGetter();
        videoGetter.batchDownload(path);
        System.out.println("Download accomplish");
    }
}
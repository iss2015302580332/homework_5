import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by WeiZehao on 16/12/12.
 * 本类用于从B站每个类别中各爬取1000页视频信息
 * 把所有视频信息存储到数据库中
 */
public class InfoGetter
{
    private static final String basicURL = "http://api.bilibili.com/archive_rank/getarchiverankbypartion?" +
            "callback=?&type=jsonp";
    private static final String tidPath = "doc/tid3.txt";
    private HttpResponse response;
    private Database database;

    public InfoGetter()
    {
        database = new Database();
    }

    /**
     * 执行爬虫，并将数据插入数据库
     */
    public void getVideoInfo()
    {
        File file = new File(tidPath);
        Scanner scanner = null;
        try
        {
            scanner = new Scanner(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        while (scanner.hasNext())
        {
            int tid = scanner.nextInt();
            System.out.println(String.format("--------获取类别：%d--------", tid));

            // 每一个类别爬取1000页视频数据
            for(int i = 0; i < 1000; i++)
            {
                System.out.println(String.format("--------获取第%d页--------", i+1));
                String url = String.format(basicURL + "&tid=%d&pn=%d",tid,i+1);
                String content = null;
                try
                {
                    // 每爬一页休息1秒
                    Thread.sleep(1000);
                    // 获取js文件
                    response = this.getRequest(url);
                    if(response == null)
                        continue;
                    HttpEntity resEntity = response.getEntity();
                    content = EntityUtils.toString(resEntity);
                    // 解析json，并将视频数据插入数据库
                    database.insertData(this.analyseJson(content));
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        database.close();
    }

    /**
     * 请求js文件获取json
     * @param url js文件的地址
     * @return json格式信息
     */
    private HttpResponse getRequest(String url)
    {
        HttpResponse response = null;
        try
        {
            HttpClient httpClient = HttpClients.createDefault();
            HttpGet request = new HttpGet(url);
            request.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            request.addHeader("Accept-Encoding", "gzip, deflate, sdch");
            request.addHeader("Accept-Language", "zh-CN,zh;q=0.8");
            request.addHeader("Content-Type", "text/html; charset=UTF-8");
            request.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " +
                    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36");
            response = httpClient.execute(request);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            return response;
        }
    }

    /**
     * 解析json信息并获取到每一个视频的av号、类别、作者、硬币数、收藏数
     * @param content json信息
     * @return 包含视频信息的对象的list
     */
    private ArrayList<Video> analyseJson(String content)
    {
        ArrayList<Video> videos = new ArrayList<Video>();
        try
        {
            JSONObject json_all = new JSONObject(content);
            JSONObject json_data = new JSONObject(json_all.get("data").toString());
            JSONObject json_archives = new JSONObject(json_data.get("archives").toString());

            // 解析一页上的视频数据（1页有20个视频）
            for(Integer i = 0; i < 20; i++)
            {
                try
                {
                    JSONObject archive = new JSONObject(json_archives.get(i.toString()).toString());
                    int aid = Integer.parseInt(archive.get("aid").toString());
                    int tid = Integer.parseInt(archive.get("tid").toString());
                    String tname = archive.get("tname").toString();
                    String author = archive.get("author").toString();

                    JSONObject stat = new JSONObject(archive.get("stat").toString());
                    int coin = Integer.parseInt(stat.get("coin").toString());
                    int favorite = Integer.parseInt(stat.get("favorite").toString());

                    videos.add(new Video(aid,tid,tname,author,coin,favorite));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return videos;
    }
}

/**
 * 视频类
 * 储存视频的av号、类别、作者、硬币数、收藏数
 */
class Video
{
    public int aid;
    public int tid;
    public String tname;
    public String author;
    public int coin;
    public int favorite;

    Video(int aid,int tid, String tname, String author, int coin, int favorite)
    {
        this.aid = aid;
        this.tid = tid;
        this.tname = tname;
        this.author = author;
        this.coin = coin;
        this.favorite = favorite;
    }
}
